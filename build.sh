export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
export PATH=$JAVA_HOME/bin:$PATH

# clean
rm -rf build
mkdir build

#compile
find src -name *.java > build/javafiles.txt
mkdir build/classes
javac -d build/classes -cp lib/controlsfx-8.0.2-developer-preview-2.jar @build/javafiles.txt

#copy resources
cd src
for e in `find . -not -name *.java -type file -not -name .DS_Store`
do cp $e ../build/classes/$e
done
cd ..

#create the list of samples
cd src
grep -lR " implements Sample " * | grep samples > ../build/classes/org/controlsfx/samples/samples.txt
cd ..

#jar
# javafx packaging has it's own stuff
#jar cf build/HelloControlsFX.jar -C build/classes .

#run
#java -cp build/HelloControlsFX.jar:lib/controlsfx-8.0.2-developer-preview-2.jar org.controlsfx.HelloControlsFX

############################
# typical build ends here  #
# JavaFX Packaging follows #
############################

#
#compile CSS
#
javafxpackager -createbss -outdir build/classes -srcdir src -srcfiles `cd src; find . -name *.css`


#
#Create the JavaFX Jar
#
mkdir build/dist
cp lib/* build/dist

javafxpackager -createjar -v \
  -appclass org.controlsfx.HelloControlsFX \
  -srcdir build/classes \
  -outfile HelloControlsFX.jar \
  -outdir build/dist \
  -nocss2bin \
  -classpath controlsfx-8.0.2-developer-preview-2.jar \
  -mainfestAttrs Permissions=sandbox 

#unused parameters:
#  -preloader <preloader class>
#          qualified name of the preloader class to be executed.
#  -paramfile <file>
#          properties file with default named application parameters.
#  -argument arg
#          An unnamed argument to be put in <fx:argument> element in the JNLP
#          file.
#  -manifestAttrs <manifest attributes>
#          List of additional manifest attributes. Syntax: "name1=value1,
#          name2=value2,name3=value3.
#  -noembedlauncher 
#          If present, the packager will not add the JavaFX launcher classes
#          to the jarfile.
#  -nocss2bin
#          The packager won't convert CSS files to binary form before copying
#          to jar. 
#  -runtimeversion <version> 
#          version of the required JavaFX Runtime.
#  -srcfiles <files>
#          List of files in srcdir. If omitted, all files in srcdir (which
#          is a mandatory argument in this case) will be packed.

#
#Sign the Jar the JavaFX way
#

# for demo purposes, use a temp self signed key
# Remember, self signed keys are basically useless.
keytool -genkey -keyalg RSA -alias useless -keystore build/tmp.jks -storepass PlaintextPasswordsAreBad -validity 1 -keysize 2048 -dname ou=useles -keypass PlaintextPasswordsAreBad

javafxpackager -signJar -v \
  -keyStore build/tmp.jks \
  -alias useless \
  -storePass PlaintextPasswordsAreBad \
  -keyPass PlaintextPasswordsAreBad \
  -outdir build/dist \
  -srcdir build/dist \

#unused parameters:
#  -srcfiles <files>
#          List of files in srcdir. If omitted, all files in srcdir (which
#          is a mandatory argument in this case) will be signed.

#
#Create the deployment
#
mkdir build/deploy

javafxpackager -deploy -v \
  -title "Hello ControlsFX" \
  -vendor "FX Experience" \
  -description "Demo Application for ControlsFX" \
  -appclass org.controlsfx.HelloControlsFX \
  -native installer \
  -name "HelloControlsFX" \
  -outdir build/deploy \
  -outfile HelloControlsFX \
  -srcdir build/dist \ 


#Options for deploy command include:
#  -preloader <preloader class>
#          qualified name of the preloader class to be executed.
#  -paramfile <file>
#          properties file with default named application parameters.
#  -htmlparamfile <file>
#          properties file with parameters for the resulting applet.
#  -width <width>
#          width of the application.
#  -height <height>
#          height of the application.
#  -embedjnlp
#          If present, the jnlp file will be embedded in the html document.
#  -embedCertificates
#          If present, the certificates will be embedded in the jnlp file.
#  -allpermissions
#          If present, the application will require all security permissions 
#          in the jnlp file.
#  -updatemode <updatemode>
#          sets the update mode for the jnlp file.
#  -isExtension
#          if present, the srcfiles are treated as extensions.
#  -callbacks
#          specifies user callback methods in generated HTML. The format is
#          "name1:value1,name2:value2,..."
#  -templateInFilename
#          name of the html template file. Placeholders are in form of
#          #XXXX.YYYY(APPID)#
#  -templateOutFilename
#          name of the html file to write the filled-in template to.
#  -templateId
#          Application ID of the application for template processing.
#  -argument arg
#          An unnamed argument to be put in <fx:argument> element in the JNLP
#          file.
#  -srcfiles <files>
#          List of files in srcdir. If omitted, all files in srcdir (which
#          is a mandatory argument in this case) will be used.




